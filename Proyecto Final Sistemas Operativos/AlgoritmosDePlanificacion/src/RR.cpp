#include "RR.h"

struct procesoRr{
    double proceso,llegada,timeExe1,timeIO,timeExe2,completado1,completado2,llegada2,real1,real2,prioridad,
           tiempoServicio,tiempoEspera,indiceServicio,tiempoEjecucion;

}procesosrr[10];
int cantProcesos3,quanto,aleatorio3;

void RR::rr(int cantProcesos3){
    struct procesoRr temp;
    for(int i=0;i<cantProcesos3-1;i++){
        for(int j=i+1;j<cantProcesos3;j++){
            if(procesosrr[i].llegada>procesosrr[j].llegada){
                    temp=procesosrr[i];
                    procesosrr[i]=procesosrr[j];
                    procesosrr[j]=temp;
                }
        }
    }
}

void RR::insertar(int cantProcesos3,int aleatorio3,int quantum){
	double tPromFinal,tPromServicio,tPromEspera,indPromServicios;
	int tiempofinal = procesosrr[0].llegada;
    if(aleatorio3==1){
        for(int i=0;i<cantProcesos3;i++){
                    procesosrr[i].proceso = i+1;
                    procesosrr[i].llegada = rand()%29;
                    procesosrr[i].prioridad = rand()%9+1;
                    procesosrr[i].timeExe1 = rand()%29+1;
                    procesosrr[i].timeIO = rand()%29+1;
                    procesosrr[i].timeExe2 = rand()%29+1;

                    tiempofinal = 0;
                    tPromFinal = 0;
                    tPromServicio = 0;
                    tPromEspera = 0;
                    indPromServicios = 0;

                    quanto = quantum;
                    procesosrr[i].llegada2=99999;
                    procesosrr[i].completado1=0;
                    procesosrr[i].completado2=0;
                    procesosrr[i].real1 = procesosrr[i].timeExe1;
                    procesosrr[i].real2 = procesosrr[i].timeExe2;
                }

    }else if(aleatorio3==2){
        for(int i=0;i<cantProcesos3;i++){
                    procesosrr[i].proceso=i+1;
                    cout<<"Proceso "<< i+1 <<endl;
                    cout<<"Ingrese el tiempo de llegada : ";
                    cin>>procesosrr[i].llegada;
                    cout<<"Ingrese la prioridad del proceso : ";
                    cin>>procesosrr[i].prioridad;
                    cout<<"Ingrese el tiempo de ejecucion de las rafagas 1 de CPU: ";
                    cin>>procesosrr[i].timeExe1;
                    cout<<"Ingrese el tiempo de ejecucion de las rafagas de E/S : ";
                    cin>>procesosrr[i].timeIO;
                    cout<<"Ingrese el tiempo de ejecucion de la rafaga 2 de CPU: ";
                    cin>>procesosrr[i].timeExe2;

                    tiempofinal = 0;
                    tPromFinal = 0;
                    tPromServicio = 0;
                    tPromEspera = 0;
                    indPromServicios = 0;

                    quanto = quantum;
                    procesosrr[i].llegada2=99999;
                    procesosrr[i].completado1=0;
                    procesosrr[i].completado2=0;
                    procesosrr[i].real1 = procesosrr[i].timeExe1;
                    procesosrr[i].real2 = procesosrr[i].timeExe2;
                }
    }
	rr(cantProcesos3);
	bool stop = false;

    cout<<"\n\t\t Algoritmo de Planificacion RR: ROUND ROBIN"<<endl<<endl;
    cout<<"|-----------|------------|-------------|--------------|-----------|--------------|----------|-------------|-----------|-------------|"<<endl;
    cout<<"|  Proceso  | T. Llegada |  Prioridad  | T. Ejecucion |    E/S    | T. Ejecucion | T. Final | T. Servicio | T. Espera | I. Servicio |"<<endl;

    do
    {
        int aux = 0;
        float rrr = -99999;
        float temp = 0;
        int loc;
        for(int i=0;i<cantProcesos3;i++)
        {
            if((procesosrr[i].completado1==0)  && (procesosrr[i].llegada <= tiempofinal))
            {
                temp = procesosrr[i].prioridad;
                if(rrr < temp)
                {
                    rrr = temp;
                    loc = i;
                    aux = 1;
                }

            }

            else if((procesosrr[i].completado2==0)  && (procesosrr[i].llegada2 <= tiempofinal))
            {
                temp = procesosrr[i].prioridad;
                if(rrr < temp)
                {
                    rrr = temp;
                    loc = i;
                    aux = 2;
                }
            }

            else if(procesosrr[i].timeIO>procesosrr[i].timeExe2 && i==(cantProcesos3-1)){
                tiempofinal += 1;
            }
        }

        if(aux == 1)
        {
            if(quanto <= procesosrr[loc].real1)
            {
              tiempofinal += quanto;
              procesosrr[loc].real1 -= quanto;

              if(procesosrr[loc].real1<=0)
              {
                  procesosrr[loc].completado1 = 1;
                  procesosrr[loc].llegada2 = tiempofinal + procesosrr[loc].timeIO ;
              }
            }

            else if(quanto > procesosrr[loc].real1)
            {
                procesosrr[loc].completado1 = 1;
                tiempofinal += procesosrr[loc].real1;
                procesosrr[loc].llegada2 = tiempofinal + procesosrr[loc].timeIO;
            }
        }

        else if(aux == 2)
        {
            if(quanto <= procesosrr[loc].real2)
            {
              tiempofinal += quanto;
              procesosrr[loc].real2 -= quanto;

              if(procesosrr[loc].real2<=0)
              {
                procesosrr[loc].completado2 = 1;
                procesosrr[loc].tiempoServicio = (tiempofinal - procesosrr[loc].llegada);
                procesosrr[loc].tiempoEspera = (procesosrr[loc].tiempoServicio - (procesosrr[loc].timeExe1 +
                                                                                  procesosrr[loc].timeExe2));
                procesosrr[loc].indiceServicio = ((procesosrr[loc].timeExe1 +
                                                   procesosrr[loc].timeExe2) /
                                                  procesosrr[loc].tiempoServicio);

                tPromFinal += tiempofinal;
                tiempofinal -= 1;
                tPromServicio += procesosrr[loc].tiempoServicio;
                tPromEspera += procesosrr[loc].tiempoEspera;
                indPromServicios += procesosrr[loc].indiceServicio;

                cout<<"|-----------|------------|-------------|--------------|-----------|--------------|----------|-------------|-----------|-------------|"<<endl;
                cout<<"|     "<<setprecision(0)<<procesosrr[loc].proceso<<"     |     "
                              <<procesosrr[loc].llegada<<"      |      "
                              <<procesosrr[loc].prioridad<<"      |      "
                              <<procesosrr[loc].timeExe1<<"       |     "
                              <<procesosrr[loc].timeIO<<"     |      "
                              <<procesosrr[loc].timeExe2<<"       |    "
                              <<tiempofinal<<"    |     "
                              <<procesosrr[loc].tiempoServicio<<"      |    "
                              <<procesosrr[loc].tiempoEspera<<"     |     "
                              <<setprecision(2)<<procesosrr[loc].indiceServicio<<"     |"<<endl;
              }
            }

            else if(quanto > procesosrr[loc].real2)
            {
                tiempofinal += procesosrr[loc].real2;
                procesosrr[loc].completado2 = 1;
                procesosrr[loc].tiempoServicio = (tiempofinal - procesosrr[loc].llegada);
                procesosrr[loc].tiempoEspera = (procesosrr[loc].tiempoServicio - (procesosrr[loc].timeExe1 +
                                                                                  procesosrr[loc].timeExe2));
                procesosrr[loc].indiceServicio = ((procesosrr[loc].timeExe1 +
                                                   procesosrr[loc].timeExe2) /
                                                  procesosrr[loc].tiempoServicio);

                tPromFinal += tiempofinal;
                tPromServicio += procesosrr[loc].tiempoServicio;
                tPromEspera += procesosrr[loc].tiempoEspera;
                indPromServicios += procesosrr[loc].indiceServicio;

                cout<<"|-----------|------------|-------------|--------------|-----------|--------------|----------|-------------|-----------|-------------|"<<endl;
                cout<<"|     "<<setprecision(0)<<procesosrr[loc].proceso<<"     |     "
                              <<procesosrr[loc].llegada<<"      |      "
                              <<procesosrr[loc].prioridad<<"      |      "
                              <<procesosrr[loc].timeExe1<<"       |     "
                              <<procesosrr[loc].timeIO<<"     |      "
                              <<procesosrr[loc].timeExe2<<"       |    "
                              <<tiempofinal<<"    |     "
                              <<procesosrr[loc].tiempoServicio<<"      |    "
                              <<procesosrr[loc].tiempoEspera<<"     |     "
                              <<setprecision(2)<<procesosrr[loc].indiceServicio<<"     |"<<endl;
            }

        }
//        stop = true;

        for(int i=0;i<cantProcesos3;i++)
        {
            stop = true;
            if(procesosrr[i].completado2 == 0)
            {
                stop = false;
                break;
            }
        }
    }while(!stop);


        tPromFinal /= cantProcesos3;
        tPromServicio /= cantProcesos3;
        tPromEspera /= cantProcesos3;
        indPromServicios /= cantProcesos3;

        cout<<"|-----------|------------|-------------|--------------|-----------|--------------|----------|-------------|-----------|-------------|"<<endl;
        cout<<"|-----------|------------|-------------|--------------|-----------|--------------|  Tiempo  |    Tiempo   |  Tiempo   |   Indice    |"<<endl;
        cout<<"|-----------|------------|-------------|--------------|-----------|--------------| Promedio |   Promedio  | Promedio  |   Promedio  |"<<endl;
        cout<<"|-----------|------------|-------------|--------------|-----------|--------------|   Final  | de Servicio | de Espera | de Servicio |"<<endl;
        cout<<"|-----------|------------|-------------|--------------|-----------|--------------|----------|-------------|-----------|-------------|"<<endl;
        cout<<"|-----------|------------|-------------|--------------|-----------|--------------|   "<<fixed << setprecision(2) <<tPromFinal<<"  |     "<<tPromServicio<<"   |   "<<tPromEspera<<"   |    "<<indPromServicios<<"     |"<<endl;
        cout<<"|-----------|------------|-------------|--------------|-----------|--------------|----------|-------------|-----------|-------------|"<<endl;
}
