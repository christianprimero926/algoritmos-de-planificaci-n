#include "FCFS.h"

struct procesoFcfs{
    int proceso,llegada,timeExe1,timeIO,timeExe2;
    double tiempoServicio,tiempoEspera,indiceServicio,tiempoEjecucion;
	double tPromFinal,tPromServicio,tPromEspera,indPromServicios;
}procesos[10];
int cantProcesos, aleatorio;

void FCFS::fcfs(int cantProcesos){
    struct procesoFcfs temp;
    for(int i=0;i<cantProcesos-1;i++){
        for(int j=i+1;j<cantProcesos;j++){
            if(procesos[i].llegada>procesos[j].llegada){
                    temp=procesos[i];
                    procesos[i]=procesos[j];
                    procesos[j]=temp;
                }
        }
    }
}

void FCFS::insertar(int cantProcesos,int aleatorio){
    double tPromFinal,tPromServicio,tPromEspera,indPromServicios;
    if(aleatorio==1){
        for(int i=0;i<cantProcesos;i++){
                    procesos[i].proceso=i+1;
                    procesos[i].llegada = rand()%29;
                    procesos[i].timeExe1 = rand()%29 +1;
                    procesos[i].timeIO = rand()%29+1;
                    procesos[i].timeExe2 = rand()%29+1;
                }

    }else if(aleatorio==2){
        for(int i=0;i<cantProcesos;i++){
                    procesos[i].proceso=i+1;
                    cout<<"Proceso "<< i+1 <<endl;
                    cout<<"Ingrese el tiempo de llegada : ";
                    cin>>procesos[i].llegada;
                    cout<<"Ingrese el tiempo de ejecucion de las rafagas 1 de CPU: ";
                    cin>>procesos[i].timeExe1;
                    cout<<"Ingrese el tiempo de ejecucion de las rafagas de E/S : ";
                    cin>>procesos[i].timeIO;
                    cout<<"Ingrese el tiempo de ejecucion de la rafaga 2 de CPU: ";
                    cin>>procesos[i].timeExe2;
                }
    }

    int tiempofinal = 0;


    fcfs(cantProcesos);

    cout<<"\n\t\t Algoritmo de Planificacion FCFS: FIRST COME FIRST SERVERED"<<endl<<endl;
    cout<<"|-----------|------------|--------------|-----------|--------------|----------|-------------|-----------|-------------|"<<endl;
    cout<<"|  Proceso  | T. Llegada | T. Ejecucion |    E/S    | T. Ejecucion | T. Final | T. Servicio | T. Espera | I. Servicio |"<<endl;

    for(int i=0; i<cantProcesos; i++) {
            tiempofinal += procesos[i].timeExe1;
    }
    for(int k=0; k<cantProcesos; k++){
            tiempofinal += procesos[k].timeExe2;
            procesos[k].tPromFinal = 0;
            procesos[k].tPromServicio = 0;
            procesos[k].tPromEspera = 0;
            procesos[k].indPromServicios = 0;

            procesos[k].tiempoServicio = tiempofinal-procesos[k].llegada;
            procesos[k].tiempoEjecucion = procesos[k].timeExe1+procesos[k].timeExe2;
            procesos[k].tiempoEspera = procesos[k].tiempoServicio - procesos[k].tiempoEjecucion;
            procesos[k].indiceServicio = procesos[k].tiempoEjecucion/procesos[k].tiempoServicio;

            procesos[k].tPromFinal += tiempofinal;
            procesos[k].tPromServicio += procesos[k].tiempoServicio;
            procesos[k].tPromEspera += procesos[k].tiempoEspera;
            procesos[k].indPromServicios += procesos[k].indiceServicio;

            tPromFinal+=procesos[k].tPromFinal;
            tPromServicio+=procesos[k].tPromServicio;
            tPromEspera+=procesos[k].tPromEspera;
            indPromServicios+=procesos[k].indPromServicios;

            cout<<"|-----------|------------|--------------|-----------|--------------|----------|-------------|-----------|-------------|"<<endl;
            cout<<"|     "<<setprecision(0)<<procesos[k].proceso<<"     |     "
                          <<procesos[k].llegada<<"      |      "
                          <<procesos[k].timeExe1<<"       |     "
                          <<procesos[k].timeIO<<"     |      "
                          <<procesos[k].timeExe2<<"       |    "
                          <<tiempofinal<<"    |     "
                          <<procesos[k].tiempoServicio<<"      |    "
                          <<procesos[k].tiempoEspera<<"     |     "
                          <<setprecision(2)<<procesos[k].indiceServicio<<"     |"<<endl;
        }

        tPromFinal /= cantProcesos;
        tPromServicio /= cantProcesos;
        tPromEspera /= cantProcesos;
        indPromServicios /= cantProcesos;

        cout<<"|-----------|------------|--------------|-----------|--------------|----------|-------------|-----------|-------------|"<<endl;
        cout<<"|-----------|------------|--------------|-----------|--------------|  Tiempo  |    Tiempo   |  Tiempo   |   Indice    |"<<endl;
        cout<<"|-----------|------------|--------------|-----------|--------------| Promedio |   Promedio  | Promedio  |   Promedio  |"<<endl;
        cout<<"|           |            |              |           |              |   Final  | de Servicio | de Espera | de Servicio |"<<endl;
        cout<<"|-----------|------------|--------------|-----------|--------------|----------|-------------|-----------|-------------|"<<endl;
        cout<<"|           |            |              |           |              |   "<<fixed << setprecision(2) <<tPromFinal<<"  |     "<<tPromServicio<<"   |   "<<tPromEspera<<"   |    "<<indPromServicios<<"     |"<<endl;
        cout<<"|-----------|------------|--------------|-----------|--------------|----------|-------------|-----------|-------------|"<<endl;


}
