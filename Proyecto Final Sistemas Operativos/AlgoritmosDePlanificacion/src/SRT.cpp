#include "SRT.h"

struct procesoSrt{
    double proceso,llegada,timeExe1,timeIO,timeExe2,completado1,completado2,llegada2,actual1,actual2,
           tiempoServicio,tiempoEspera,indiceServicio,tiempoEjecucion;
}procesossrt[10];
int cantProcesos2, aleatorio2;

void SRT::srt(int cantProcesos2){
    struct procesoSrt temp;
    for(int i=0;i<cantProcesos2-1;i++){
        for(int j=i+1;j<cantProcesos2;j++){
            if(procesossrt[i].llegada>procesossrt[j].llegada){
                    temp=procesossrt[i];
                    procesossrt[i]=procesossrt[j];
                    procesossrt[j]=temp;
                }
        }
    }
}

void SRT::insertar(int cantProcesos2,int aleatorio2){
    int tiempofinal = procesossrt[0].llegada;
    double tPromFinal,tPromServicio,tPromEspera,indPromServicios;
    if(aleatorio2==1){
        for(int i=0;i<cantProcesos2;i++){
                    procesossrt[i].proceso=i+1;
                    procesossrt[i].llegada = rand()%29;
                    procesossrt[i].timeExe1 = rand()%29 +1;
                    procesossrt[i].timeIO = rand()%29 +1;
                    procesossrt[i].timeExe2 = rand()%29 +1;

                    tiempofinal = 0;
                    tPromFinal = 0;
                    tPromServicio = 0;
                    tPromEspera = 0;
                    indPromServicios = 0;

                    procesossrt[i].actual1=0;
                    procesossrt[i].actual2=0;
                    procesossrt[i].llegada2=99999;
                    procesossrt[i].completado1=0;
                    procesossrt[i].completado2=0;
                }

    }else if(aleatorio2==2){
        for(int i=0;i<cantProcesos2;i++){
                    procesossrt[i].proceso=i+1;
                    cout<<"Proceso "<< i+1 <<endl;
                    cout<<"Ingrese el tiempo de llegada : ";
                    cin>>procesossrt[i].llegada;
                    cout<<"Ingrese el tiempo de ejecucion de las rafagas 1 de CPU: ";
                    cin>>procesossrt[i].timeExe1;
                    cout<<"Ingrese el tiempo de ejecucion de las rafagas de E/S : ";
                    cin>>procesossrt[i].timeIO;
                    cout<<"Ingrese el tiempo de ejecucion de la rafaga 2 de CPU: ";
                    cin>>procesossrt[i].timeExe2;

                    tiempofinal = 0;
                    tPromFinal = 0;
                    tPromServicio = 0;
                    tPromEspera = 0;
                    indPromServicios = 0;

                    procesossrt[i].actual1=0;
                    procesossrt[i].actual2=0;
                    procesossrt[i].llegada2=99999;
                    procesossrt[i].completado1=0;
                    procesossrt[i].completado2=0;
                }
    }
	srt(cantProcesos2);
    bool stop = false;

    cout<<"\n\t\t Algoritmo de Planificacion SRT: SHORTEST REMAINING TIME"<<endl<<endl;
    cout<<"|-----------|------------|--------------|-----------|--------------|----------|-------------|-----------|-------------|"<<endl;
    cout<<"|  Proceso  | T. Llegada | T. Ejecucion |    E/S    | T. Ejecucion | T. Final | T. Servicio | T. Espera | I. Servicio |"<<endl;



    do
    {
        int aux = 0;
        float srr = 99999;
        float temp = 0;
        int loc;
        for(int i=0;i<cantProcesos2;i++)
        {
            if((procesossrt[i].completado1==0)  && (procesossrt[i].llegada <= tiempofinal))
            {
                temp = (procesossrt[i].timeExe1 - procesossrt[i].actual1);
                if(temp < srr)
                {
                    srr = temp;
                    loc = i;
                    aux = 1;
                }

            }

            else if((procesossrt[i].completado2==0)  && (procesossrt[i].llegada2 <= tiempofinal))
            {
                temp = (procesossrt[i].timeExe2 - procesossrt[i].actual2);
                if(temp < srr)
                {
                    srr = temp;
                    loc = i;
                    aux = 2;
                }
            }
            else if(aleatorio2==1 && i==(cantProcesos2-1))
            {
                tiempofinal += 1;
            }
        }

        if(aux == 1){
            procesossrt[loc].actual1 += 1;
            tiempofinal += 1;
            if(procesossrt[loc].actual1==procesossrt[loc].timeExe1){
                procesossrt[loc].completado1 = 1;
                procesossrt[loc].llegada2 = (tiempofinal + procesossrt[loc].timeIO);

            }
        }

        else if(aux == 2)
        {
            procesossrt[loc].actual2 += 1;
            tiempofinal += 1;
            if(procesossrt[loc].actual2==procesossrt[loc].timeExe2){
                procesossrt[loc].completado2 = 1;
                procesossrt[loc].tiempoServicio = (tiempofinal - procesossrt[loc].llegada);
                procesossrt[loc].tiempoEspera = (procesossrt[loc].tiempoServicio - (procesossrt[loc].timeExe1 + procesossrt[loc].timeExe2));
                procesossrt[loc].indiceServicio = ((procesossrt[loc].timeExe1 + procesossrt[loc].timeExe2) /procesossrt[loc].tiempoServicio);

                tPromFinal += tiempofinal;
                tPromServicio += procesossrt[loc].tiempoServicio;
                tPromEspera += procesossrt[loc].tiempoEspera;
                indPromServicios += procesossrt[loc].indiceServicio;

                cout<<"|-----------|------------|--------------|-----------|--------------|----------|-------------|-----------|-------------|"<<endl;
                cout<<"|     "<<setprecision(0)<<procesossrt[loc].proceso<<"     |     "
                              <<procesossrt[loc].llegada<<"      |      "
                              <<procesossrt[loc].timeExe1<<"       |     "
                              <<procesossrt[loc].timeIO<<"     |      "
                              <<procesossrt[loc].timeExe2<<"       |    "
                              <<tiempofinal<<"    |     "
                              <<procesossrt[loc].tiempoServicio<<"      |    "
                              <<procesossrt[loc].tiempoEspera<<"     |     "
                              <<setprecision(2)<<procesossrt[loc].indiceServicio<<"     |"<<endl;
            }

        }

        for(int i=0;i<cantProcesos2;i++)
        {
            stop = true;
            if(procesossrt[i].completado2 == 0)
            {
                stop = false;
                break;
            }
        }
    }while(!stop);

        tPromFinal /= cantProcesos2;
        tPromServicio /= cantProcesos2;
        tPromEspera /= cantProcesos2;
        indPromServicios /= cantProcesos2;

        cout<<"|-----------|------------|--------------|-----------|--------------|----------|-------------|-----------|-------------|"<<endl;
        cout<<"|-----------|------------|--------------|-----------|--------------|  Tiempo  |    Tiempo   |  Tiempo   |   Indice    |"<<endl;
        cout<<"|-----------|------------|--------------|-----------|--------------| Promedio |   Promedio  | Promedio  |   Promedio  |"<<endl;
        cout<<"|           |            |              |           |              |   Final  | de Servicio | de Espera | de Servicio |"<<endl;
        cout<<"|-----------|------------|--------------|-----------|--------------|----------|-------------|-----------|-------------|"<<endl;
        cout<<"|           |            |              |           |              |   "<<fixed << setprecision(2) <<tPromFinal<<"  |     "<<tPromServicio<<"   |   "<<tPromEspera<<"   |    "<<indPromServicios<<"     |"<<endl;
        cout<<"|-----------|------------|--------------|-----------|--------------|----------|-------------|-----------|-------------|"<<endl;


}
