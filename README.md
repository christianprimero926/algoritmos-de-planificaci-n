# Algoritmos de Planificación

Proyecto Final de Sistemas Operativos, programa que simula 4 algoritmos de planificación, (2 con política apropiativa y 2 con no-apropiativa), se deben ingresar los datos por consola o se pueden generar los valores aleatorios.

Parte 1: Algoritmos de Planificación

Elabore un programa de computador (preferiblemente OO) que permita simular los algoritmos vistos en
clase para la planificación de la CPU. El programa debe cumplir con los siguientes requerimientos:
1. Debe presentar al usuario dos opciones, para:
*  Generar al azar los datos necesarios para la simulación.
*  Permitir que el usuario digite desde el teclado los datos necesarios para la simulación.
Estos datos son: número de procesos, instante de llegada (que puede ser diferente de cero),
ráfagas de CPU, ráfagas de E/S, ráfagas de CPU, prioridad, etc.
2. Debe implementar 4 algoritmos de planificación vistos en clase (2 políticas apropiativas y 2 no
apropiativas). Para un conjunto de tareas dado, deberán ejecutarse 4 simulaciones (una por algoritmo)
y presentar los resultados de las 4.
3. El programa debe presentar los resultados de cada simulación en forma tabulada (de manera que se
puedan comparar los diferentes valores resultantes) y también en forma gráfica. Concretamente: los
tiempos de espera, de servicio, instante de tiempo final y el índice de servicio para cada proceso, así
como los tiempos promedio de espera y servicio.
4. Pruebe su programa con un conjunto de 10 procesos generados al azar. Analice y explique los
resultados.